import React, { Component } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import AppMainNav from './src/navigations';

export default class App extends Component {
  render() {
    return (

      <SafeAreaView style={styles.container}>
        <AppMainNav />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});
