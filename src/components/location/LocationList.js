import React, { Component } from 'react';
import { ScrollView, View, Text, Modal } from 'react-native';
import Spinner from 'react-native-spinkit';
import LocationHeader from './LocationHeader';
import LocationCard from './LocationCard';
import LocationMeterDetail from './LocationMeterDetail';

class LocationList extends Component {

  constructor(props){
    super(props);
    this.state={
        loading: false,
        latitude: null,
        longitude: null,
        error: null
    }
  }

  componentDidMount() {
      this.setState({loading: true});
      navigator.geolocation.getCurrentPosition(
        (position) => {
          this.setState({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            error: null,
            loading: true
          });
        },
        (error) => this.setState({ error: error.message }),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
      );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <LocationHeader title={'26 Steve Road'} />
        <LocationCard />
        <View style={{
            flex: 1
            }}
        >
            {this.state.loading ?
                <View style={{flex: 1}}>
                  <Modal
                    animationType="fade"
                    transparent={true}
                  />
                <ScrollView
                contentContainerStyle={{
                  alignItems: 'center',
                  backgroundColor: '#d8dad3',
                  paddingTop: '1%',
                  paddingBottom: '1%'
                  
                }}
                style={{
                  backgroundColor: '#d8dad3',
                }}
                showsVerticalScrollIndicator={false}
                >

                    <LocationMeterDetail />
                    <LocationMeterDetail />
                    <LocationMeterDetail />
                    <LocationMeterDetail />
                    <LocationMeterDetail />
                         
                    <View style={{flex: 1, position: 'absolute', top: 0, bottom: 0, right: 0, left: 0, backgroundColor: 'black', opacity: 0.7 }}>
                    <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}> 
                          <Text style={{textAlign: 'center', fontSize: 25, fontWeight: '800', color:'white'}}>Confirming your</Text>
                          <Text style={{textAlign: 'center', fontSize: 25, fontWeight: '800', color:'white'}}>location...</Text>
                    </View> 
                      <View style={{flex: 0.8, alignItems: 'center', paddingRight: '20%'}}>
                          <Spinner style={{marginLeft:'20%'}} type={'ArcAlt'} color={'#FAA919'} size={100}/>
                      </View>
                    </View>
                </ScrollView>


                </View>
            : 
                <ScrollView
                contentContainerStyle={{
                  alignItems: 'center',
                  backgroundColor: '#d8dad3',
                  paddingTop: '1%',
                  paddingBottom: '1%'
                  
                }}
                style={{
                  backgroundColor: '#d8dad3',
                }}
                showsVerticalScrollIndicator={false}
                >
                  <LocationMeterDetail />
                  <LocationMeterDetail />
                  <LocationMeterDetail />
                  <LocationMeterDetail />
                  <LocationMeterDetail />
                </ScrollView>
            }

        </View>
      </View>
    );
  }
}

export default LocationList;
