import React from 'react';
import { View, Dimensions, Text, ImageBackground, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


import Directions  from '@webill/react-native-icons/directions';
import Cameraalt from '@webill/react-native-icons/cameraalt'
import house from '../../images/house.jpg';

const LocationCard = () => {

    const { container } = styles;  

    return(
        <View style={container}>

            <ImageBackground style={{height:Dimensions.get('window').height/2.5, width:'100%'}} source={house}>

            <View style={{width: '100%', flex: 0.7}}>
                <View style={{alignItems: 'flex-end'}}>
                    <Cameraalt />
                </View>
            </View>

            <View style={{width: '100%', justifyContent: 'flex-end', flex: 0.3, flexDirection: 'row', opacity: 0.8}}>
            
                <View style={{width: '100%', flex: 0.25}}>
                    <TouchableOpacity>
                        <Directions color={'white'} height={80} width={80}/>
                    </TouchableOpacity>
                </View>
                <View style={{width: '100%', flex: 0.8, flexDirection: 'row'}}>
                    
                    <Text style={{paddingLeft: '2%', alignSelf: 'center', color: 'white', fontWeight: 'bold', fontSize: 16}}>26 Steve Road{"\n"}Dainfern, Johannesburg{"\n"}2195</Text>

                </View>
                
                <View style={{width: '100%',  borderWidth: 1, borderColor: 'yellow', flex: 0.2}}>

                </View>

            </View>

            </ImageBackground>
        </View>
    );  
}

const styles = {
    container:{
        height: Dimensions.get('window').height/2.5,
        width: '100%'
    },
};

export default LocationCard;