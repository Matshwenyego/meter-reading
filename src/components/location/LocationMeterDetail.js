import React from 'react';
import { View, Dimensions, Text, TouchableOpacity } from 'react-native';

import Electricity from '@webill/react-native-icons/electricity';
import Sync from '@webill/react-native-icons/sync';

const LocationMeterDetail = () => {
  const {
    container,
    header,
    separator,
    wrapper,
    textWrapper,
    label,
    statistics,
    headerWrapper,
    meterNumber
  } = styles;

  return (
    <View style={container}>
      <View style={header}>
        <View style={headerWrapper}>
          <Electricity color="white" />
          <Text style={meterNumber}>{'L88A 22916 45678'}</Text>
        </View>
        <Sync color="white" />
      </View>
      <View style={separator} />
      <View style={wrapper}>
        <View style={textWrapper}>
          <Text style={label}>{'Last read:'}</Text>
          <Text style={statistics}>{'22-12-2017'}</Text>
        </View>
        <View style={textWrapper}>
          <Text style={label}>{'Read by:'}</Text>
          <Text style={statistics}>{'Jacob Gitswart'}</Text>
        </View>
        <View style={textWrapper}>
          <Text style={label}>{'Previous reading (kWh):'}</Text>
          <Text style={statistics}>{'120 345,8'}</Text>
        </View>
        <View style={textWrapper}>
          <Text style={label}>{'Current reading (kWh):'}</Text>
          <Text style={statistics}>{'-'}</Text>
        </View>
        <View style={textWrapper}>
          <Text style={label}>{'3 Month Average Usage:'}</Text>
          <Text style={statistics}>{'988'}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = {
  container: {
    // height: '45%',
    // width: '95%',
    height: Dimensions.get('window').height/3.9,
    width: Dimensions.get('window').width - 15,
    backgroundColor: 'white',
    marginTop: '1%',
    marginBottom: '1%',
  },
  header: {
    height: '24%',
    width: '100%',
    backgroundColor: '#13ce66',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  separator: {
    backgroundColor: '#d8dad3',
    height: '3%',
    width: '100%'
  },
  wrapper: {
    flexDirection: 'column',
    width: '100%',
    paddingHorizontal: '2%',
    // paddingTop: '2%',
  },
  label: {
    paddingVertical: '0.5%',
    fontSize: 15,
    color: '#404041',
    opacity: 0.8,
    fontWeight: '400',
  },
  statistics: {
    paddingVertical: '0.5%',
    fontSize: 15,
    color: 'black',
    fontWeight: '400'
  },
  meterNumber: {
    color: 'white',
    fontSize: 20
  },
  textWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingVertical: '0.5%'
  },
  headerWrapper: {
    width: '70%',
    // justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  }
};

export default LocationMeterDetail;
