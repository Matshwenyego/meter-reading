import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Spinner from 'react-native-spinkit';

class LocationGeolocation extends Component {

    constructor(props){
        super(props);
        this.state={
            loading: false,
            latitude: null,
            longitude: null,
            error: null
        }
    }

    componentDidMount() {
        this.setState({loading: true});
        navigator.geolocation.getCurrentPosition(
          (position) => {
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              error: null,
              loading: false
            });
          },
          (error) => this.setState({ error: error.message }),
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }
    
    render() {
    return (
        <View style={{ flex: 1, opacity: 0.5, backgroundColor: 'rgba(0,0,0,0.5)'}}>
            {this.state.loading ?
                <View style={{flex: 1}}>
                    <View style={{flex: 0.4, alignItems: 'center', justifyContent: 'center'}}> 
                        <Text style={{textAlign: 'center', fontSize: 25, fontWeight: '800', color:'white'}}>Confirming your</Text>
                        <Text style={{textAlign: 'center', fontSize: 25, fontWeight: '800', color:'white'}}>location...</Text>
                    </View>
                    <View style={{flex: 0.6, alignItems: 'center', paddingRight: '20%'}}>
                        <Spinner style={{marginLeft:'20%'}} type={'ArcAlt'} color={'#FAA919'} size={100}/>
                    </View>
                </View>
            : 
                <View>
                    <Text style={{color: 'white'}}>Latitude: {this.state.latitude}</Text>
                    <Text style={{color: 'white'}}>Longitude: {this.state.longitude}</Text>
                    {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
                </View>
            }

        </View>
    );
    }
}


export default LocationGeolocation;
