import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import addDays from 'date-fns/add_days';
import eachDay from 'date-fns/each_day';
import format from 'date-fns/format';
import CalendarHeader from './CalendarHeader';
import DateList from './DateList';
import DateGrid from './DateGrid';

export default class Calendar extends Component {

    today = new Date();
    dateList = this.getDates();
    weekDayNames = this.getWeekDayNames(this.dateList);

    constructor(props){
        super(props);
        this.state = {clicked: false}
        this.getDateItems();
    }

    toggle = () => {
        this.setState({
            clicked: !this.state.clicked,
        })
    }

    getDateItems() {
        const dates = this.dateList.map((date, index) => {
            return  {
                key: index,
                value: {
                    day: format(date, 'D'),
                    weekDayName: this.weekDayNames[index],
                }
            }
        });
        return dates;
    }

    getWeekDayNames() {
        return this.dateList.map(k => format(k,'ddd'));
    }

    getDates() {
        const dateAWeekFromNow = addDays(this.today, 6);

        const thisWeek = eachDay( this.today , dateAWeekFromNow );

        return thisWeek;
    }

    render() {
        return (
            <View style={styles.container}>
                {this.state.clicked? 
                    <View> 
                        <CalendarHeader month={format(this.today, 'MMMM')} year={format(this.today, 'YYYY')} onPress={this.toggle.bind(this)} name={"clock"}/>
                        <DateGrid /> 
                    </View> 
                    : 
                    <View> 
                        <CalendarHeader month={format(this.today, 'MMMM')} year={format(this.today, 'YYYY')} onPress={this.toggle.bind(this)} name={"calendar"}/>
                        <DateList dateItems={this.getDateItems()} /> 
                    </View>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
   container: {
       height: 'auto',
       width: '100%',
       backgroundColor: '#F1F1F2',
       position: 'relative'
   }
});
