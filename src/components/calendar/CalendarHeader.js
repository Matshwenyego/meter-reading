import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';


const CalendarHeader = ({ month, year, onPress, name }) => {
    return (
            <View style={styles.container}>
                <View style={styles.dateWrapper}>
                    <Text style={styles.dateText}>Schedule - {month} {year}</Text>
                </View>
                <View style={styles.iconWrapper}>
                    <TouchableOpacity onPress={onPress}>
                        <Icon color="white" size={30} name={name}/>
                    </TouchableOpacity>
                </View>
            </View>
        );
};

export default CalendarHeader;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#404041',
        height: 'auto',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        shadowColor: '#000000',
        shadowOpacity: 1,
        shadowOffset: { width: 2, height: 4},
        shadowRadius: 1,
        elevation: 2
    },
    dateText: {
        fontSize: 14,
        fontWeight: '500',
        opacity: 0.85,
        color: 'white',
        textAlign: 'center',
        paddingTop: '3%'
    },
    iconWrapper: {
        justifyContent: 'center',
        height:'auto'
    },
    dateWrapper: {
        marginRight: '22%',
        marginTop: '-1%',
    }
});