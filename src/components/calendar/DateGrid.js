import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Calendar } from 'react-native-calendars'
import moment from 'moment';

export default class DateGrid extends Component{

    constructor(props){
        super(props);
        this.state={
            today: moment(new Date()).format('YYYY-MM-DD'),
            selectedDay: []
        };
    }

    render(){
        return(
            <View style={styles.container}>
                <Calendar
                    onDayPress={(day) => {  this.setState({selectedDay: moment(day).format('YYYY-MM-DD')}) }}
                    hideArrows={true}
                    firstDay={1}
                    hideExtraDays={true}
                    markedDates={{
                        [this.state.today]:{selected: true, disableTouchEvent: false, selectedColor: '#FAA919',},
                        [this.state.selectedDay]:{selected: true, disableTouchEvent: false, selectedColor: '#FFFFFF'},
                        '2018-07-01':{marked: true, dotColor: '#FFFFFF'},
                        '2018-07-10':{marked: true, dotColor: '#13CE66'},
                        '2018-07-11':{marked: true, dotColor: '#13CE66'}
                    }}
                    theme={{
                        calendarBackground:'#404041',
                        dayTextColor: 'white',
                        todayTextColor: 'black',

                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        shadowColor: '#000000',
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 3},
        shadowRadius: 1,    
    }
});

