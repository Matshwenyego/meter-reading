import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';


export default class DateListItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            position: props.dateItem.index
        };

        
    }

    onUpdateStatus = () => {
        this.props.updateStatus(this.state.position);

        // Make request to load schedules when clicked - watch realm videos
    };

    render() {
        const dayStyles = {
            fontWeight: this.props.fontWeight,
            color: this.props.color
        };

        return (
            <TouchableOpacity onPress={this.onUpdateStatus} >
                <View style={styles.container}>
                    <Text style={[styles.dayText, dayStyles, {opacity: 0.7}]}>{this.props.dateItem.item.value.weekDayName}</Text>
                    <Text style={[styles.dateText, dayStyles, {opacity: 0.8}]}>{this.props.dateItem.item.value.day}</Text>
                </View>
            </TouchableOpacity>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        marginVertical: 5,
        width: Dimensions.get('window').width/7.1,
        height: Dimensions.get('window').width/7,
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative'
    },
    dayText: {
        fontSize: 14
    },
    dateText: {
        fontSize: 22,
        paddingVertical: '10%'
    }
});