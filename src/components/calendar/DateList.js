import React, { Component } from 'react';
import { View, StyleSheet, FlatList, Animated, Dimensions, Platform }  from 'react-native';

import DateListItem from './DateListItem';

export default class DateList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            translate: new Animated.ValueXY({x: 0, y: 0}),
            dateItems: props.dateItems,
            selectedIndex: 0
        };
    }

    onUpdateStatus = (val) => {
        this.setState({
           selectedIndex: val
        });
        Animated.timing(this.state.translate, {
            toValue: {x: val * Dimensions.get('window').width/7, y: 0},
            useNativeDriver: true
        }).start();
    };

    render() {

        const translateStyle = this.state.translate.getTranslateTransform();

        return (

            <View style={styles.container}>
                <FlatList
                    renderItem={(item) => {
                        if(item.index === this.state.selectedIndex) {
                            return (<DateListItem fontWeight={"700"}
                                                  color={"black"}
                                                  opacity={1}
                                                  dateItem={item}
                                                  updateStatus={this.onUpdateStatus}/>)
                        }
                        return (<DateListItem fontWeight={"300"} dateItem={item} color={"black"} updateStatus={this.onUpdateStatus}/>) }
                    }
                    horizontal={true}
                    data={this.state.dateItems}
                    ItemSeparatorComponent={() =>
                        <View style={{
                            borderColor: 'gray',
                            borderStyle: 'solid',
                            alignSelf: 'center',
                            borderLeftWidth: 1,
                            height: 15,
                            opacity: 0.5
                        }}/>
                    }
                    scrollEnabled={false}
                    keyExtractor={(item, index) => index.toString()}
                    extraData={this.state.selectedIndex}
                    showsHorizontalScrollIndicator={false}
                />
                <Animated.View style={[styles.overlay, {transform: translateStyle}]} />
            </View>

        );
    };
}

const styles = StyleSheet.create({
    container: {
        elevation:5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: "100%",
        backgroundColor: '#F1F1F2',
        ...Platform.select({
            ios: {
                shadowColor: '#000000',
                shadowOffset: {
                    width: 2, height: 3
                },
                shadowRadius: 1,
                shadowOpacity: 0.2
            }
        })
    },
    overlay: {
        backgroundColor: '#404041',
        opacity: 0.1,
        width: Dimensions.get('window').width/7 + 2 ,
        height: Dimensions.get('window').width/5.8,
        position: 'absolute'
    }
});