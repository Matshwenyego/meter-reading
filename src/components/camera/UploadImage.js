import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    SafeAreaView,
    Dimensions
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Svg,{
    Circle,
    Rect,
    Defs,
    ClipPath,
    Line
} from 'react-native-svg';

export default class UploadImage extends Component {
    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={styles.container}>

                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={{flex: 1}}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.auto}
                    autoFocus={RNCamera.Constants.AutoFocus.on}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}>
                    <View style = {styles.preview}>
                        <Icon name="cross" size={35} color="white"/>
                        <MaterialIcons name="flash-auto" size={30} color="white" style={styles.autoFocus}/>
                    </View>

                    <View style={{top: 40}}>
                        <TouchableOpacity
                            onPress={this.takePicture.bind(this)}
                        >
                            <Svg
                                height={400}
                                width={400}
                            >
                                <Line
                                    x1="70"
                                    y1="60"
                                    x2="130"
                                    y2="60"
                                    stroke="white"
                                    strokeWidth="3"
                                />

                                <Line
                                    x1="70"
                                    y1="120"
                                    x2="70"
                                    y2="60"
                                    stroke="white"
                                    strokeWidth="3"
                                />

                                <Line
                                    x1="240"
                                    y1="60"
                                    x2="300"
                                    y2="60"
                                    stroke="white"
                                    strokeWidth="3"
                                />

                                <Line
                                    x1="300"
                                    y1="60"
                                    x2="300"
                                    y2="120"
                                    stroke="white"
                                    strokeWidth="3"
                                />

                                <Line
                                    x1="70"
                                    y1="240"
                                    x2="70"
                                    y2="300"
                                    stroke="white"
                                    strokeWidth="3"
                                />

                                <Line
                                    x1="70"
                                    y1="300"
                                    x2="130"
                                    y2="300"
                                    stroke="white"
                                    strokeWidth="3"
                                />

                                <Line
                                    x1="240"
                                    y1="300"
                                    x2="300"
                                    y2="300"
                                    stroke="white"
                                    strokeWidth="3"
                                />

                                <Line
                                    x1="300"
                                    y1="240"
                                    x2="300"
                                    y2="300"
                                    stroke="white"
                                    strokeWidth="3"
                                />
                            </Svg>
                        </TouchableOpacity>
                    </View>

                    <View style={{top: 30}}>
                        <TouchableOpacity
                            onPress={this.takePicture.bind(this)}
                            style = {styles.capture}>
                        </TouchableOpacity>
                    </View>
                </RNCamera>

                </View>
            </SafeAreaView>
        );
    }

    takePicture = async function() {
        if (this.camera) {
            const options = { quality: 40.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            console.log(data.uri);

        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    preview: {
        marginTop: '1%',
        paddingLeft: '1%',
        paddingRight: '1%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    capture: {
        backgroundColor: '#fff',
        padding: 15,
        paddingHorizontal: 40,
        alignSelf: 'center',
        margin: 40,
        width:80,
        height:80,
        borderRadius: 40,
        backgroundColor: '#faa919',
        borderColor: 'white',
        borderWidth: 2,
        position: 'relative'
    },
    boxFrame: {
        padding: 15,
        paddingHorizontal: 40,
        top: 40,
        alignSelf: 'center',
        margin: 40,
        width:'80%',
        height:'55%',
        borderRadius: 8,
        borderColor: 'white',
        borderWidth: 4,
        backgroundColor: 'transparent',
    }

});
