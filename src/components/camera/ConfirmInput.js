import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';

export default class ConfirmInput extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: ''
        }
    }

    _updateMeterValue = (val) => {
        this.setState({
            text: `${this.state.text}${val}`
        });
    };

    _removeDigit = () => {
        this.setState({
           text: this.state.text.slice(0, this.state.text.length - 1)
        });
    };

    render() {
        return (
              <View style={styles.container}>
                  <View style={styles.picture}>
                    <View style={{height: 40, flexDirection: 'row', justifyContent: 'space-between', paddingTop: '2%'}}>

                        <View style={{flexDirection: 'row'}}>
                            <TouchableOpacity>
                                <Entypo style={{paddingLeft: '2%'}} name='chevron-left' color='white' size={30}  />
                            </TouchableOpacity>
                            <Text style={{paddingTop: '1%', paddingLeft: '1%', color: 'white', fontWeight: '800', fontSize: 20}}>Retake</Text>
                        </View>
                        <TouchableOpacity>
                            <Ionicons style={{paddingRight: '2%'}} name='md-trash' color='red' size={30}  />
                        </TouchableOpacity>
                    
                    </View>

                    <View style={{justifyContent: 'flex-end', marginTop: '22%', width: 290, alignSelf: 'center'}}>
                        <Text style={styles.pictureText}>Confirm image reading with manual input.</Text>
                    </View>

                  </View>
                  <View style={{position: 'absolute', top: '32%', left: 0, right: 0, bottom: 0, padding: '1%'}}>
                      <LinearGradient
                          colors={['#FFFFFF50','#FFFFFF', '#FFFFFF',  '#FFFFFF']}
                          style={{width: '90%',
                              alignSelf: 'center',
                              height: '98%',
                              paddingTop: '4%',
                              paddingBottom: '2%',
                              borderRadius: 5
                            }}
                      >

                      <View style={styles.confirmInput}>
                          <View style={{padding: '0.7%'}}></View>
                              <View style={styles.textInput}>
                                  <Text style={styles.inputText}>{this.state.text}</Text>
                              </View>
                              <View style={{height: '65%', width: '95%', alignSelf: 'center'}}>
                                  <View style={{padding: '1.5%'}}></View>
                                  <Grid>
                                      <Row>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(1)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>1</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(2)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>2</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(3)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>3</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                      </Row>

                                      <Row>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(4)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>4</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(5)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>5</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(6)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>6</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                      </Row>
                                      <Row>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(7)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>7</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(8)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>8</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(9)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>9</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                      </Row>
                                      <Row>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue('.')}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>.</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._updateMeterValue(0)}>
                                                  <View>
                                                      <Text style={styles.numPadValue}>0</Text>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                          <Col>
                                              <TouchableOpacity style={styles.numPadButton} onPress={() => this._removeDigit()}>
                                                  <View>
                                                    <Feather name='delete' color='#faa919' size={25}/>
                                                  </View>
                                              </TouchableOpacity>
                                          </Col>
                                      </Row>
                                  </Grid>

                              </View>
                          <TouchableOpacity onPress={this._onConfirmButtonPress}>
                              <View style={styles.confirmButton}>
                                  <Text style={styles.confirmButtonText}>Confirm Manual Input</Text>
                              </View>
                          </TouchableOpacity>
                      </View>
                      </LinearGradient>
                  </View>
              </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#D8DAD3'
    },
    picture: {
        width: '100%',
        height: '45%',
        backgroundColor: 'orange',
    },
    pictureText: {
        justifyContent: 'flex-end',
        fontSize: 22,
        color: 'white',
        fontWeight: '600',
        textAlign: 'center'
    },
    confirmInput: {
        width: '92%',
        alignSelf: 'center',
        height: '100%',
        borderRadius: 2.5,
        borderWidth: 5,
        backgroundColor: 'black'
    },
    textInput: {
      width: '97%',
      alignSelf: 'center',
      height: '20%',
      backgroundColor: 'white',
      alignItems: 'flex-end',
        justifyContent: 'center'

    },
    confirmButton: {
        width: '92%',
        height: '35%',
        backgroundColor: '#faa919',
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'

    },
    confirmButtonText: {
        fontSize: 22,
        color: 'white',
        fontWeight: '700'
    },
    numPadButton: {
        width: '90%',
        height: '85%',
        justifyContent: 'center',
        alignItems:'center',
        backgroundColor: '#404041',
        alignSelf: 'center'
    },
    numPadValue: {
        fontSize: 18,
        fontWeight: '400',
        color: '#faa919'
    },
    inputText: {
      fontSize: 50,
      fontWeight: '800',
      color: 'black'
    }
});

