import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

class DirectionsMap extends Component{

    constructor(props){
        super(props);
        this.state = {
            loaded: false,
            latitude: null,
            longitude: null,
            error: null,
            focusedLocation:{
                latitude: -26.171691,
                longitude: 28.132749,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }
        }
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            this.setState({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              error: null,
              loaded: true
            });
            console.log('latitude', this.state.latitude);
            console.log('longitude', this.state.longitude);
          },
          (error) => this.setState({ error: error.message }),
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    }


    render(){
        return(
            <View style={{flex: 1}}>

                {this.state.loaded ?
                    <MapView
                        style={styles.mapStyle}
                        region={this.state.focusedLocation}
                    >
                        
                        <MapView.Marker coordinate={{latitude: this.state.latitude, longitude: this.state.longitude}}/>
                        <MapView.Marker coordinate={{latitude:-26.184691,longitude:28.132749}}/>
                        <MapViewDirections
                            origin={{latitude: this.state.latitude, longitude: this.state.longitude}}
                            destination={{latitude:-26.184691,longitude:28.132749}}
                            apikey={'AIzaSyDYAeTTUsYQpyctj3UHzH2NtOBMNUFQrjI'}
                            strokeWidth={3}
                            strokeColor='red'
                        />
                    </MapView>
                :
                    null
                }

            </View>
        );
    }
};

const styles = StyleSheet.create({
    mapStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    }
});

export default DirectionsMap;