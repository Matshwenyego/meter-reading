import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

import DirectionsHeader from './DirectionsHeader';
import DirectionsMap from './DirectionsMap';

class DirectionsList extends Component{

    render(){
        return(
            <View style={{flex: 1}}>

                <DirectionsHeader title={'Directions'}/>
                <DirectionsMap />

            </View>
        );
    }
};

const styles = StyleSheet.create({
    mapStyle: {
        position: 'absolute',
        top: '6%',
        left: 0,
        right: 0,
        bottom: 0,
    }
});

export default DirectionsList;