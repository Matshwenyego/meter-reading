import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

import Electricity from '../../images/electricity.svg';
import Water from '../../images/water.svg';

class FeedCard extends Component{
    constructor(props){
        super(props);
        this.state={
            ...props,
            isPressed: false
        }
    }

    toggle = () => {
        this.setState({
            isPressed: !this.state.isPressed
        })
    }

    render(){
        return (
            <View style={styles.container}>
                {this.state.isPressed?
                <TouchableOpacity onPress={this.toggle.bind(this)}>
                    <View style={{flexDirection: 'row', height: 60, borderRadius: 2, borderColor: '#13CE66'}}>

                        <View style={{flex: 0.15, backgroundColor: '#13CE66', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{textAlign: 'center', alignContent: 'center', color: '#FFFFFF'}}>{this.state.sites}</Text>
                        </View>

                        <View style={{flex: 0.85, flexDirection:'row', borderWidth: 1, borderColor:'#13CE66', borderLeftWidth: 0}}>

                            <View style={{flex:0.8375, justifyContent: 'center'}}>

                                <View style={{paddingLeft: '4%'}}>
                                    <Text style={{color: '#13CE66', fontWeight: '400'}}>{this.state.streetname}</Text>
                                    <Text style={{color: '#13CE66', fontWeight: '400'}}>{this.state.suburb}</Text>
                                </View>

                            </View>

                            <View style={{flex:0.2125, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                                
                                <Icon name="power-plug" size={20} color={'#13CE66'}/>
                                <Icon name="water" size={20} color={'#13CE66'}/>

                            </View>

                        </View>
                    </View>
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={this.toggle.bind(this)}>
                    <View style={{flexDirection: 'row', height:60, borderRadius: 2, }}>

                        <View style={{flex: 0.15, backgroundColor: '#13CE66', justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{textAlign: 'center', alignContent: 'center', color: '#FFFFFF'}}>{this.state.sites}</Text>
                        </View>

                        <View style={{flex: 0.85, flexDirection:'row'}}>

                            <View style={{flex:0.8375, justifyContent: 'center'}}>

                                <View style={{paddingLeft: '4%'}}>
                                    <Text style={{color: '#000000'}}>{this.state.streetname}</Text>
                                    <Text style={{color: '#000000'}}>{this.state.suburb}</Text>
                                </View>

                            </View>

                            <View style={{flex:0.2125, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>

                                <Icon name="power-plug" size={20} color={'#000000'}/>
                                <Icon name="water" size={20} color={'#000000'}/>

                            </View>

                        </View>
                    </View>
                </TouchableOpacity>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        flex: 1,
        marginVertical: '1%',
        shadowColor: '#000000',
        shadowOffset: {width: 2, height: 4},
        shadowOpacity: 0.2,
    }
});

export default FeedCard;
