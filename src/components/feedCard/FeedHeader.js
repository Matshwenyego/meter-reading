import React from 'react';
import { View, Text, TouchableOpacity} from 'react-native';
import Chevronleft from '@webill/react-native-icons/chevronleft';

const FeedHeader = ({ title, onPressNav, onPressMap, children}) => {
    const { headerContainer, text } = styles;

    return(
        <View style={headerContainer}>

            <View style={{paddingLeft: '2%', justifyContent: 'center'}}>
                <TouchableOpacity onPress={onPressNav}>
                    <Chevronleft color={'#FFFFFF'}/>
                </TouchableOpacity>
            </View>

            <Text style={text}>{title}</Text>

            <View style={{paddingRight: '2%', justifyContent: 'center'}}>
                <TouchableOpacity onPress={onPressMap}>
                    {children}
                </TouchableOpacity>
            </View>
        </View>
    );  
}

const styles = {
    headerContainer:{
        backgroundColor: '#404041',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 40,
        shadowColor: '#000000',
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 4 },
        elevation: 2,
    },
    text:{
        paddingBottom: '2%',
        fontSize: 18,
        color: '#FAA919',
        justifyContent: 'center',
        textAlign: 'center',
        paddingTop: '2%',
    },
};

export default FeedHeader;
