import React from 'react';
import { View, TextInput, Platform, StyleSheet } from 'react-native';
import Search from '@webill/react-native-icons/search';


const FeedSearch = () => {
    const { container, search, icon, searchContainer, iconContainer, searchBoxContainer } = styles;

    return(
        <View style={container}>

            <View style={searchBoxContainer}>
                <View style={searchContainer}>
                    <TextInput
                        style={search}
                        underlineColorAndroid={'transparent'}
                    />
                </View>

                <View style={iconContainer}>
                    <Search />
                </View>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: '2%',
        shadowColor: '#000000',
        shadowOffset: {width: 2, height: 4},
        shadowOpacity: 0.2,

    },
    searchBoxContainer:{
        backgroundColor: '#FFFFFF',
        flexDirection: 'row', 
        alignSelf: 'center', 
        width: 'auto'
    },
    searchContainer: {
        borderColor: '#000000',
        width: '86%',
        alignSelf: 'flex-start',
        height: 35
    },
    search: {
        paddingTop: '3%',
        height: Platform.OS === 'ios' ? 28 : 36,
        paddingLeft: '3%',
        fontWeight: '400',
    },
    iconContainer: {
        borderColor: '#000000',
        alignSelf: 'flex-end',
    },
    icon:{
        color: '#000000',
        alignSelf: 'flex-end',
    },
});

export default FeedSearch;