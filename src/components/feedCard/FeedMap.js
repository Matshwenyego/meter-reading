import React, { Component } from 'react';
import { View, TouchableOpacity, StyleSheet, Platform, Image, Text, } from 'react-native';
import MapView from 'react-native-maps';
import Icon from 'react-native-vector-icons/Entypo';
import red from '../../images/icons/siteindicatorred.png';
import green from '../../images/icons/siteindicatorgreen.png';
import yellow from '../../images/icons/siteindicatoryellow.png';
import grey from '../../images/icons/siteindicatorgrey.png';

const READ = [
    {
      key: 'Cafe Sydney',
      title: 'Cafe Sydney',
      description: 'Customs House, 31 Alfred St, Bedfordview NSW 2000',
      latLong: {
        latitude: -26.184691,
        longitude: 28.132749,
      },
    },
    {
      key: 'Four Frogs Creperie',
      title: 'Four Frogs Creperie',
      description: '1 Macquarie Pl, Bedfordview NSW 2000',
      latLong: {
        latitude: -26.181091,
        longitude: 28.132749,
      },
    },

  ];

  const UNREAD = [
    {
        key: 'Tapavino',
        title: 'Tapavino',
        description: '6 Bulletin Pl, Bedfordview NSW 2000',
        latLong: {
          latitude: -26.203180,
          longitude: 28.132749,
        },
    },
  ];

  const PENDING = [
    {
        key: 'McDonalds',
        title: 'McDonalds',
        description: '17 Bulletin Pl, Bedfordview NSW 2000',
        latLong: {
          latitude: -26.195291,
          longitude: 28.132749,
        },
    },
  ]



class FeedMap extends Component {

    constructor(props){
        super(props);
        this.state = {
            ...props,
            minus: false, 
            plus: false,
            focusedLocation:{
                latitude: -26.171691,
                longitude: 28.132749,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }
        }
    }


    onPressZoomIn = () => {
        this.region = {
          latitude: this.state.focusedLocation.latitude,
          longitude: this.state.focusedLocation.longitude,
          latitudeDelta: this.state.focusedLocation.latitudeDelta / 5,
          longitudeDelta: this.state.focusedLocation.longitudeDelta / 5
        }
    
        this.setState({
          focusedLocation: {
            latitudeDelta: this.region.latitudeDelta,
            longitudeDelta: this.region.longitudeDelta,
            latitude: this.region.latitude,
            longitude: this.region.longitude
          }
        })
        this.map.animateToRegion(this.region, 100);
      }

      onPressZoomOut = () => {
        this.region = {
          latitude: this.state.focusedLocation.latitude,
          longitude: this.state.focusedLocation.longitude,
          latitudeDelta: this.state.focusedLocation.latitudeDelta * 5,
          longitudeDelta: this.state.focusedLocation.longitudeDelta * 5
        }
        this.setState({
          focusedLocation: {
            latitudeDelta: this.region.latitudeDelta,
            longitudeDelta: this.region.longitudeDelta,
            latitude: this.region.latitude,
            longitude: this.region.longitude
          }
        })
        this.map.animateToRegion(this.region, 100);
      }

    onPressMarker(e, index) {
        this.setState({selectedMarkerIndex: index});
    }


    render(){
        return(
            <View style={{flex: 1}}>

                <View style={styles.container}>
                    <MapView
                        style={styles.mapStyle}
                        region={this.state.focusedLocation}
                        
                        ref={ref => this.map = ref}
                    >
                        {
                            READ.map((mark, index) => 
                                <MapView.Marker
                                    coordinate={mark.latLong}
                                    title={mark.title}
                                    description={mark.description}
                                    key={`marker-${index}`}
                                >
                                    <View>
                                        <Image source={green} style={{height: 50, width: 50}}/>
                                        <Text style={{color: '#FFFFFF', fontWeight: '500', marginTop: -35,alignSelf: 'center'}}>1</Text>
                                    </View>

                                </MapView.Marker>
                            )

                        }

                        {
                            UNREAD.map((mark, index) => 
                                <MapView.Marker
                                    coordinate={mark.latLong}
                                    title={mark.title}
                                    description={mark.description}
                                    key={`marker-${index}`}
                                >
                                    <View>
                                        <Image source={red} style={{height: 50, width: 50}}/>
                                        <Text style={{color: '#FFFFFF', fontWeight: '500', marginTop: -35,alignSelf: 'center'}}>2</Text>
                                    </View>

                                </MapView.Marker>
                            )

                        }

                        {
                            PENDING.map((mark, index) => 
                                <MapView.Marker
                                    coordinate={mark.latLong}
                                    title={mark.title}
                                    description={mark.description}
                                    key={`marker-${index}`}
                                >
                                    <View>
                                        <Image source={yellow} style={{height: 50, width: 50}}/>
                                        <Text style={{color: '#FFFFFF', fontWeight: '500', marginTop: -35,alignSelf: 'center'}}>1</Text>
                                    </View>

                                </MapView.Marker>
                            )

                        }
                    </MapView>
                </View>

                <View style={styles.zoom}>
                    <View style={{ borderWidth: 1, borderRadius: 6, backgroundColor: '#FFFFFF', width: 50, height: 90, justifyContent: 'center', alignItems: 'center', paddingTop: Platform.OS === 'ios' ? '1%' : 0, }}>
                        <TouchableOpacity onPress={this.onPressZoomIn.bind(this)}>
                            <Icon name={'plus'} size={35} color={'#000000'}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.onPressZoomOut.bind(this)}>
                            <Icon name={'minus'} size={35} color={'#000000'}/>
                        </TouchableOpacity>
                    </View>

                </View>

            </View>
        )
    }
}

const styles =  StyleSheet.create({
    container: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    mapStyle: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },
    zoom: {
        flex: 1,
        paddingRight: '4%',
        paddingBottom: '4%',
        alignSelf: 'flex-end',
        justifyContent: 'flex-end',
    }
  });

export default FeedMap;