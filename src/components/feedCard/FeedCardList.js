import React, { Component } from 'react';
import { StyleSheet, View, FlatList, } from 'react-native';
import FeedCard from './FeedCard';
import FeedHeader from './FeedHeader';
import FeedSearch from './FeedSearch';
import FeedMap from './FeedMap';
import Map from '@webill/react-native-icons/map';
import List from '@webill/react-native-icons/list'


export default class FeedCardList extends Component {

    constructor(props){
        super(props);
        this.state={clicked: false}
    }

    toggleMap = () => {
        console.log('Inside');
        this.setState({
            clicked:!this.state.clicked
        })
    }

    render() {
        return (
            <View style={styles.container}>
                {this.state.clicked? 
                    <View style={{flex: 1}}>
                        <FeedHeader                 
                            title={'Schedule 987567'}
                            navIcon={'chevron-left'} 
                            onPressNav={() => null}
                            children={<List color={'#FFFFFF'}/>}
                            onPressMap={this.toggleMap.bind(this)}
                        />
                        <FeedMap />
                    </View>
                :   

                    <View style={{flex: 1}}>
                        <FeedHeader                 
                            title={'Schedule 987567'}
                            navIcon={'chevron-left'} 
                            onPressNav={() => null}
                            children={<Map color={'#FFFFFF'}/>}
                            onPressMap={this.toggleMap.bind(this)}
                        />
                        <FeedSearch />
                        <View style={styles.flatlistContainer}>
                            <FlatList
                                removeClippedItems={true}
                                showsVerticalScrollIndicator={false}
                                data={['1', '2', '3', '4', '5', '6']}
                                renderItem={({item}) => <FeedCard sites={'12'} streetname={'7 Maartens St'} suburb={'Dainfern'} onPress={() => null}/> }
                                keyExtractor={(item) => item}
                            />
                        </View>
                    </View>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        backgroundColor: '#d8dad3',
        flex: 1
    },
    flatlistContainer: {
        flex: 1,
        paddingLeft: 8,
        paddingRight: 8,
        paddingVertical: '2%',
        paddingHorizontal: '2%',
    }
});
