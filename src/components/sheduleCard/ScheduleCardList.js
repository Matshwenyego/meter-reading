import React, { Component } from 'react';
import { StyleSheet, View, Text, FlatList, ActivityIndicator }  from 'react-native';
import ScheduleCard from './ScheduleCard';


export default class ScheduleCardList extends Component {

    constructor(props){
        super();
        this.state = {
            isLoading: true
        }
    }

    componentDidMount() {
        return fetch('http://localhost:3000/schedules')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson,
                }, function() {
                    // do something with new state
                });
            })
            .catch((error) => {
                console.log(error);
                throw error;
            });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.spinner}>
                    <ActivityIndicator  size={'large'}/>
                </View>
            );
        }

        return (
            <View style={styles.container}>
                <FlatList
                    removeClippedItems={true}
                    showsVerticalScrollIndicator={false}
                    data={this.state.dataSource}
                    renderItem={({item}) => <ScheduleCard  {...item}/> }
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: '2%',
    },
    spinner: {
        flex: 1,
        paddingTop: '50%',
    }
});