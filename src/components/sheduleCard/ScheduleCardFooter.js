import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Electricity from '@webill/react-native-icons/electricity';
import Water from '@webill/react-native-icons/water';

export default class ScheduleCardFooter extends Component {

    constructor(props) {
        super(props);

        this.state = {
            ...props
        };
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <View style={styles.container}>
                    <View style={styles.sitesWrapper}>
                        <View>
                            <Text style={styles.siteAndMeterCount}>{this.state.numberOfSites}</Text>
                        </View>
                        <View style={styles.labelAndKMWrapper}>
                            <Text style={styles.sitesLabel}>sites</Text>
                            <Text style={styles.totalKM}>{this.state.routeDistance} km</Text>
                        </View>
                    </View>
                    <View style={styles.meterWrapper}>
                        <View style={styles.meterCountWrapper}>
                            <Electricity />
                            <Water />
                        </View>
                        <Text style={styles.siteAndMeterCount}>{this.state.totalMeters}</Text>
                    </View>
                </View>

                <View style={styles.divider}/>

                <View style={styles.textWrapper}>
                    <Text style={styles.textSize}>SITES</Text>
                    <Text style={styles.textSize}>TEAM PROGRESS STATS</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        paddingTop: '5%',
        paddingLeft: '2%'
    },
    sitesWrapper: {
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    meterWrapper: {
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingRight: '2%'
    },
    siteAndMeterCount: {
        fontSize: 35,
        fontWeight: '800'
    },
    labelAndKMWrapper: {
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        paddingLeft: '2%'
    },
    meterCountWrapper: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        paddingRight: '2%',
        paddingTop: '5%',
    },
    sitesLabel: {
        color: '#D8DAD3',
        textAlign: 'left'
    },
    totalKM: {
        color: 'green',
        fontSize: 15,
        fontWeight: '600'
    },
    divider:{
        borderBottomWidth: 1, 
        borderBottomColor: '#FAA919'
    },
    textWrapper:{
        flexDirection: 'row', 
        paddingLeft: '8%', 
        paddingRight: '8%', 
        paddingTop: '2%', 
        justifyContent: 'space-between'
    },
    textSize:{
        fontSize: 18,
    }

});