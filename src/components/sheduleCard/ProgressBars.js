import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';

const ONE_HUNDRED = 100;
const GREEN_COLOR_CODE = '#13CE66';
const RED_COLOR_CODE = '#FF4949';
const GRAY_COLOR_CODE = '#D8DAD3';


export default class ProgressBars extends Component {

    constructor(props) {
        super(props);

        this.state = {
            ...props
        };
    }

    countMetersBasedOnStatus = (status) => {
        return this.state.sites.reduce((acc, value) => {
            const meters = value.meters.filter(m => m.status === status);
            return acc + meters.length;
        },0);
    };

    countAllMeters = () => {
        return this.state.sites.reduce((acc, value) => {
            return acc + value.meters.length;
        },0);
    };

    calculateLineWidth = (color) => {
        if (color === 'green') {
            return (this.countMetersBasedOnStatus('read') / this.state.numberOfMeters ) * ONE_HUNDRED;
            
        } else if (color === 'gray') {
            return (this.countMetersBasedOnStatus('pending') / this.state.numberOfMeters ) * ONE_HUNDRED;
        } else {
            return (this.countMetersBasedOnStatus('unread') / this.state.numberOfMeters ) * ONE_HUNDRED;
        }
    };

    buildProgressBar = ({color, position, alignItems, alignSelf, justifyContent, width, height}) => {
        return (
            <View style={{backgroundColor: color, position: position, alignItems: alignItems, alignSelf: alignSelf, justifyContent: justifyContent, width: width, height: height}}>
                {(
                    () => {
                        if(this.countMetersBasedOnStatus('pending') !== this.countAllMeters()) {
                            return (
                                <View style={[styles.meterCountCircle, {width:this.state.circleDimensions.width * 2, height:this.state.circleDimensions.height, borderRadius: this.state.circleDimensions.height/2,
                                    backgroundColor: color}]} >

                                    {(
                                        () => {
                                            let status, meterCountTextColor;

                                            if(color === GRAY_COLOR_CODE) {
                                                status = 'pending';
                                                meterCountTextColor = 'black';
                                            } else {
                                                meterCountTextColor = 'white';
                                                status = (color === GREEN_COLOR_CODE) ? 'read': 'unread';
                                            }

                                            return (
                                                <Text style={{color: meterCountTextColor, fontSize: this.state.fontSize}}>
                                                    {this.countMetersBasedOnStatus(status)}
                                                </Text>
                                            )
                                        }
                                    )()}
                                </View>
                            )
                        }
                    })()}
            </View>
        )
    };

    configureProgressBarProperties = (color, width) => {
        switch (color) {
            case 'green':
                return {
                    color: GREEN_COLOR_CODE,
                    position: 'absolute',
                    alignItems: 'flex-end',
                    alignSelf: 'flex-start',
                    justifyContent: 'center',
                    width: width,
                    height: this.state.height
                };

            case 'red':
                return {
                    color: RED_COLOR_CODE,
                    position: 'absolute',
                    alignItems: 'flex-start',
                    alignSelf: 'flex-end',
                    justifyContent: 'center',
                    width: width,
                    height: this.state.height
                };

            case 'gray':
                return {
                    color: GRAY_COLOR_CODE,
                    position: 'absolute',
                    alignItems: 'center',
                    alignSelf: 'auto',
                    justifyContent: 'center',
                    width: width,
                    height: this.state.height
                };
        }
    };

    createProgressBar = (color) => {
        let width = this.calculateLineWidth(color);
        if ( width > 0) {

            if (color !== 'gray') {
                const grayWidth = this.calculateLineWidth('gray');

                if (grayWidth > 0) {
                    width -= 2;
                }
            } else {
                width += this.calculateLineWidth('green') * 2;
            }

            const properties = this.configureProgressBarProperties(color, `${width}%`);
            return (this.buildProgressBar({...properties}));
        }
    };

    render() {
        return (
            <View style={[styles.container, {height: this.state.height}]}>
                {this.createProgressBar('gray')}
                {this.createProgressBar('green')}
                {this.createProgressBar('red')}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        borderBottomColor: GRAY_COLOR_CODE,
        // position: 'relative',
        
        

    },
    meterCountCircle: {
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute'
    }
});
