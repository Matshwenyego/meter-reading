import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import ScheduleCardHeader from './ScheduleCardHeader';
import ScheduleCardFooter from './ScheduleCardFooter';
import ProgressBars from './ProgressBars';

// const PROGRESS_BAR_SCALE = 0.012;
const PROGRESS_BAR_CIRCLE_SCALE = 0.065;
// const PROGRESS_BAR_FONT_SCALE = 0.025;

export default class ScheduleCard extends Component {


    constructor(props) {
        super(props);
        this.state = {
            ...props
        };
    }

    countMetersBasedOnStatus = (status) => {
        return this.state.sites.reduce((acc, value) => {
            const meters = value.meters.filter(m => m.status === status);
            return acc + meters.length;
        } , 0);
    };

    getTotalNumberOfMeters = () => {
      return this.state.sites.reduce((acc, val) => {
          return val.numberOfMeters + acc
      } , 0);
    };

    calculateCircleDimensions = (height, width) => {
        return {
            height: (Dimensions.get('screen').height * PROGRESS_BAR_CIRCLE_SCALE) - 5,
            width:  ((Dimensions.get('screen').height * PROGRESS_BAR_CIRCLE_SCALE) / 2) - 2.5,
            
        }
    };

    percentage(){
        const total = Math.round((this.countMetersBasedOnStatus('read') / this.getTotalNumberOfMeters()) * 100);
        if(total == 100){
            return 'Complete'
        } else {
            return (total + '% Complete');
        }
    }


    render() {
        const {width, height} = Dimensions.get('screen');
        
        return (
            <View style={styles.container}>
                    <ScheduleCardHeader number={this.state.number} startDate={this.state.startDate} endDate={this.state.endDate} totalMeters={this.percentage()}/>
                    <ProgressBars sites={this.state.sites} numberOfMeters={this.getTotalNumberOfMeters()} height={8} circleDimensions={this.calculateCircleDimensions(height, width)} fontSize={16}/>
                    <ScheduleCardFooter numberOfSites={this.state.sites.length} routeDistance={this.state.routeDistance} totalMeters={this.getTotalNumberOfMeters()}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container: {
        height: 190,
        backgroundColor: 'white',
        marginVertical: '2%',
        shadowColor: '#000000',
        shadowOpacity: 0.2,
        shadowOffset: { width: 2, height: 3},
        borderRadius: 1,
   }
});