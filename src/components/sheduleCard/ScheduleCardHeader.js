import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import parse from 'date-fns/parse';
import differenceInCalendarDays from 'date-fns/difference_in_calendar_days';

export default class ScheduleCardHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ...props
        };
    }

    calculateDueDate = () => {
        const currentDate = new Date();
        const endDate = parse(this.state.endDate, 'DD-MM-YYYY', new Date());
        
        if (endDate.getDay() === currentDate.getDay() && endDate.getMonth() === currentDate.getMonth() && endDate.getYear() === currentDate.getYear()) {   
            return `today`;   
        }
        else {
            
            return differenceInCalendarDays(endDate, currentDate);
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Text style={styles.scheduleText}>Schedule {this.state.number}</Text>
                    <Text style={styles.dateText}>{this.state.startDate} - {this.state.endDate}</Text>
                </View>
                    <View>
                        <View>
                            {(
                                () => {
                                    
                                   const due = this.calculateDueDate();
                                   if (due === 'today') {
                                       return (<View><Text style={styles.dueDate}>due {due} </Text></View>);
                                   } else {
                                       return (
                                           <View>
                                               <Text style={styles.dueDate}>due in <Text style={styles.boldDueDate}>{due}</Text> day(s)</Text>
                                            </View>
                                        );
                                   }
                                }
                            )()}
                        </View>
                        <Text style={styles.dateText}>{this.state.totalMeters}</Text>
                    </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
   container: {
        backgroundColor: '#404041',
        height: '42%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        paddingHorizontal: '2%',
        paddingVertical: '4%'
   } ,
    scheduleText: {
        fontSize: 25,
        fontWeight: '500',
        color: '#FAA919'
    },
    dateText: {
        fontSize: 14,
        fontWeight: '500',
        color: 'white',
        alignSelf: 'flex-end',
        paddingRight: '1%',
    },
    boldDueDate: {
        fontWeight: '800'
    },
    dueDate: {
        alignSelf: 'flex-start',
        fontSize: 18,
        fontWeight: '500',
        color: '#FAA919'
    }

});