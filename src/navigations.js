import React from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Calendar from './components/calendar/Calendar';
import ScheduleScreen from './screens/ScheduleScreen';
import ScheduleList from './screens/ScheduleList';
import ConfirmInput from './screens/ScheduleCamera';
import ScheduleCamera from './screens/ScheduleCamera';
import ScheduleLocation from './screens/ScheduleLocation';
import ScheduleDirections from './screens/ScheduleDirections'

const TAB_ICON_SIZE = 40;

const HomeNav = createStackNavigator({
    // Camera: {
    //     screen: ScheduleCamera
    // },
    Directions: {
        screen: ScheduleDirections
    }
},{
    headerMode: 'none'
})

const Tabs = createBottomTabNavigator({
    // Schedule: {
    //     screen: ScheduleScreen,
    //     navigationOptions: () => ({
    //         tabBarIcon: (
    //             <Icon color="#FAA919" size={TAB_ICON_SIZE} name="clock" />
    //         )
    //     })
    // },
    // Schedule: {
    //     screen: ScheduleList,
    //     navigationOptions: () => ({
    //         tabBarIcon: (
    //             <Icon color="#FAA919" size={TAB_ICON_SIZE} name="clock" />
    //         )
    //     })
    // },
    Schedule: {
        screen: ScheduleLocation,
        navigationOptions: () => ({
            tabBarIcon: (
                <Icon color="#FAA919" size={TAB_ICON_SIZE} name="clock" />
            )
        })
    },


}, {
    lazy: true,
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    tabBarOptions: {
        showIcon: true,
        showLabel: false,
        activeTintColor: '#FAA919',
        inactiveTintColor: '#F1F1F2',
        style: {
            backgroundColor: '#404041',
            height: '7%'
        }
    }
});

const AppMainNav = createStackNavigator({
    Home: {
        screen: Tabs
    },
},{
    headerMode: 'none'
});

export default AppMainNav;
