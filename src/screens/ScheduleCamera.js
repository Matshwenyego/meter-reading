import React, { Component } from 'react';
import { View } from 'react-native';
import UploadImage from '../components/camera/UploadImage';


export default class ScheduleCamera extends Component {
    render() {
        return (
            <View style={{flex: 1}} >
                <UploadImage />
            </View>
        );
    }
}