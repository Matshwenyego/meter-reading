import React, { Component } from 'react';
import { View } from 'react-native';
import FeedCardList from '../components/feedCard/FeedCardList';


export default class ScheduleList extends Component {
    render() {
        return (
            <View style={{flex: 1}} >
                <FeedCardList />
            </View>
        );
    }
}