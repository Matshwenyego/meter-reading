import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';

import ScheduleCardList from '../components/sheduleCard/ScheduleCardList';
import Calendar from '../components/calendar/Calendar';

export default class ScheduleScreen extends Component {
    render() {
        return (
            <View style={{flex: 1}}>
                <Calendar />
                <ScrollView>  
                    <ScheduleCardList />
                </ScrollView>
            </View>
        );
    }
}


