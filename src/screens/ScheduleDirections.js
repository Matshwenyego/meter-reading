import React, { Component } from 'react';
import { View } from 'react-native';
import DirectionsList from '../components/directions/DirectionsList';


export default class ScheduleCamera extends Component {
    render() {
        return (
            <View style={{flex: 1}} >
                <DirectionsList />
            </View>
        );
    }
}