import React, { Component } from 'react';
import { View } from 'react-native';
import LocationList from '../components/location/LocationList';


export default class ScheduleLocation extends Component {
    render() {
        return (
            <View style={{flex: 1}} >
                <LocationList />
            </View>
        );
    }
}